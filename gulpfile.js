'use strict';

var gulp = require('gulp');
var tsc = require('gulp-tsc');
var del = require('del');
var mkdirp = require('mkdirp');

var options = {
  src: "src",
  bin: "bin",
  contents: "contents"
}

gulp.task('build', function() {
  gulp.src([options.src + '/**/*.ts'])
    .pipe(tsc({ target: "ES5", removeComments: false }))
    .pipe(gulp.dest(options.bin + '/'));
});

gulp.task('run', function() {
  // アプリ実行
  require("./bin/app");
})

gulp.task('init', function() {
  mkdirp.sync(options.contents + '/src');
  mkdirp.sync(options.contents + '/tar');
  mkdirp.sync(options.contents + '/sql');
});

gulp.task('clean', function() {
  del.sync([options.bin + '/']);
});

gulp.task('default', ['clean', 'init'], function() {
  gulp.start('build');
});
