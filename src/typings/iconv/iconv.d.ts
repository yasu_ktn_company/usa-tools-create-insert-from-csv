declare module "iconv" {
  export class Iconv {
    constructor(from: string, to: string);
    convert(buffer: Buffer): Buffer;
  }
}
