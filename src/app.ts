/// <reference path="../typings/main.d.ts" />
/// <reference path="./typings/main.d.ts" />

import { tools as rnmct } from "./tools/remove-not-master-csv";
var RemoveNotMasterCSV = rnmct.RemoveNotMasterCSV;
import { tools as ctit } from "./tools/csv-to-insert";
var CsvToInsert = ctit.CsvToInsert;
import { tools as fdt } from "./tools/file-diff";
var FileDiff = fdt.FileDiff;

// マスタ以外のCSVを削除
RemoveNotMasterCSV.removeFilesInDirectory("contents/src/");
RemoveNotMasterCSV.removeFilesInDirectory("contents/tar/");

// 差分チェック
var fd = new FileDiff();
var diffMap = fd.diff("contents/src/", "contents/tar/");
var inDirPath: string[] = [];
for (var key in diffMap) {
  if (diffMap[key].isDir2 && !diffMap[key].same) {
    inDirPath.push("contents/tar/" + key);
  }
}

// 差分のあったマスタの入替SQLファイルを出力
CsvToInsert.createInsertFileFromCsvFile(inDirPath, "contents/sql/", true);
// SQLファイルの出力結果
console.log("\n----\noutput sql count: " + inDirPath.length);
