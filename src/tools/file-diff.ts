import * as fs from "fs";

export namespace tools {
  "use strict";

  export class Diff {
    isDir1: boolean = false;
    isDir2: boolean = false;
    same: boolean = false;
  }

  export class FileDiff {
    diff = (dir1: string, dir2: string): {[key: string]: Diff;} => {
      var diffMap: {[key: string]: Diff;} = {};

      var files1 = fs.readdirSync(dir1);
      files1.forEach((file) => {
        if(!diffMap[file]) {
          diffMap[file] = new Diff();
        }
        diffMap[file].isDir1 = true;
      });

      var files2 = fs.readdirSync(dir2);
      files2.forEach((file) => {
        if(!diffMap[file]) {
          diffMap[file] = new Diff();
        }
        diffMap[file].isDir2 = true;

        if (!diffMap[file].isDir1 || !diffMap[file].isDir2) {
          return;
        }

        // ファイルの内容チェック
        var dir1FileStr = fs.readFileSync(dir1 + file).toString();
        var dir2FileStr = fs.readFileSync(dir2 + file).toString();
        if (dir1FileStr === dir2FileStr) {
          diffMap[file].same = true;
        }
      });

      return diffMap;
    }
  }
}
