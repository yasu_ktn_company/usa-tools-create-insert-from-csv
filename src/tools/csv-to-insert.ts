import * as fs from "fs";
import * as path from "path";
import * as del from "del";
import * as iconv from "iconv";
var Iconv = iconv.Iconv;

export namespace tools {
  "use strict";

  export class CsvToInsert {
    static createInsertFileFromCsvFileDir = (inDirPath: string, outDirPath: string, outputDeleteSql: boolean = false) => {
      var inFiles = fs.readdirSync(inDirPath);
      var inFilePath: string[] = [];
      inFiles.forEach((file) => {
        inFilePath.push(inDirPath + file);
      });
      CsvToInsert.createInsertFileFromCsvFile(inFilePath, outDirPath, outputDeleteSql);
    }

    static createInsertFileFromCsvFile = (inFilePath: string[], outDirPath: string, outputDeleteSql: boolean = false) => {
      del.sync([outDirPath + '**/*']);
      inFilePath.forEach((file) => {
        var rtn = CsvToInsert.createInsertStrFromCsvFile(file, path.basename(file, ".csv"));
        var data = "";
        if (outputDeleteSql) {
          data += CsvToInsert.createDeleteStr(path.basename(file, ".csv")) + "\r\n";
        }
        rtn.forEach((elm) => {
          data += elm + "\r\n";
        });
        fs.writeFileSync(outDirPath + path.basename(file, ".csv") + ".txt", data);
      });
    }

    static createInsertStrFromCsvFile = (path: string, tableName: string): string[] => {
      var lines = CsvToInsert.readCsvFileObj(path);

      var insertStrs: string[] = [];
      lines.forEach((line) => {
        var keys: string[] = [];
        var values: string[] = [];
        for (var key in line) {
          keys.push("[" + key + "]");
          values.push(CsvToInsert.getValueStr(line[key]));
        }
        var insertStr =
          "INSERT INTO [dbo].[" + tableName + "] "
          + "(" + keys.toString() + ") "
          + "VALUES (" + values.toString() + ");";

        insertStrs.push(insertStr);
      })

      return insertStrs;
    }

    private static readCsvFile = (path: string): any[] => {
      var linesObj = [];
      var tmp = CsvToInsert.fileReadConvUtf8(path)
        .replace(/\r\n/g, "\n")
        .replace(/, *,/g, ",null,")
        .replace(/\n *,/g, "\nnull,")
        .replace(/, *\n/g, ",null\n")
        .replace(/true/ig, "true")
        .replace(/false/ig, "false")
      var lines = tmp.replace(/, *,/g, ",null,").split("\n");

      lines.forEach((line) => {
        if (line.trim() !== "") {
          var lineObj = JSON.parse("[" + line + "]");
          linesObj.push(lineObj);
        }
      })
      return linesObj;
    }

    private static readCsvFileObj = (path: string): any[] => {
      var lines = CsvToInsert.readCsvFile(path);

      var labels: string[];
      var colObjs: any[] = [];
      lines.forEach((line, idx) => {
        if (idx === 0) {
          labels = line;
          return;
        }

        var colObj: {} = {};
        line.forEach((column: string, idx: number) => {
          colObj[labels[idx]] = column;
        })
        colObjs.push(colObj);
      });
      return colObjs;
    }

    private static createDeleteStr = (tableName: string): string => {
      return "DELETE FROM [dbo].[" + tableName + "];"
    }

    private static getValueStr = (value: any): string => {
      if (value === null) {
        return "NULL";
      }
      if (typeof(value) === "boolean") {
        return value ? "1" : "0";
      }
      if (typeof(value) === "string") {
        return "N'" + value + "'";
      }
      return value;
    }

    private static fileReadConvUtf8 = (path: string, encoding: string = "SHIFT-JIS"): string => {
      var buffer = fs.readFileSync(path);
      var ic = new Iconv(
        encoding,
        'UTF-8//TRANSLIT//IGNORE');
      return ic.convert(buffer).toString();
    }
  }
}
