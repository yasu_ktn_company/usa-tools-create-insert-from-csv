import * as fs from "fs";

export namespace tools {
  "use strict";

  export class RemoveNotMasterCSV {
    static removeFilesInDirectory = (dir: string) => {
      var ciFiles = fs.readdirSync(dir);
      ciFiles.forEach((file) => {
        if (!RemoveNotMasterCSV.strContain(file, "Master.csv")) {
          console.log("rm " + dir + file);
          fs.unlinkSync(dir + file);
        }
      });
    }

    private static strContain = (src: string, target: string): boolean => {
      return src.indexOf(target) > 0;
    }
  }
}
